Talk to House
-------------

This is an application that creates a chatbot.
The chatbot takes on a provided persona, the default being "a house".
You can then have an informal chat with the persona.

### Requirements

This requires python 3.9 and poetry.

### Running

You can run the core chatbot in text only mode with `make text`.
You can talk to the chatbot and hear the responses with `make speech`.

The speech to text works substantially better in a quiet room.

#### Changing the Persona

The core commands to run the chatbot are:

```shell
poetry run python -m src.main text
```

and

```shell
poetry run python -m src.main speech
```

These two modes accept a `--personality` flag which takes a brief description of the persona of the talker.
The chatbot, and thus the personas, were trained using the [PERSONACHAT dataset](https://arxiv.org/pdf/1801.07243.pdf).
An example persona from that dataset:

```
To me, there is nothing like a day at the seashore.
My father sales vehicles for a living.
I love to pamper myself on a regular basis.
I need to lose weight.
I am into equestrian sports.
```

You can pass this to the chatbot as follows:

```shell
poetry run python -m src.main text --personality "To me, there is nothing like a day at the seashore. My father sales vehicles for a living. I love to pamper myself on a regular basis. I need to lose weight. I am into equestrian sports."
```
