import wave
import io

import pyaudio


def play_sound(data: str) -> None:
    wavfile = wave.open(io.BytesIO(data))

    player = pyaudio.PyAudio()

    try:
        stream = player.open(
            format=player.get_format_from_width(wavfile.getsampwidth()),
            channels=wavfile.getnchannels(),
            rate=wavfile.getframerate(),
            output=True,
        )

        try:
            data = wavfile.readframes(1024)
            while data:
                stream.write(data)
                data = wavfile.readframes(1024)
        finally:
            stream.stop_stream()
            stream.close()
    finally:
        player.terminate()
