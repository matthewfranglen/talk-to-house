import io
from typing import Optional

import librosa
import speech_recognition as sr


def listen(phrase_time_limit: Optional[float] = 5.0) -> sr.AudioData:
    recognizer = sr.Recognizer()
    with sr.Microphone() as source:
        return recognizer.listen(source, phrase_time_limit=phrase_time_limit)


def to_wav(audio: sr.AudioData) -> str:
    return audio.get_wav_data()


def resample(audio: str, sampling_rate: int = 16_000) -> str:
    return librosa.load(io.BytesIO(audio), sr=sampling_rate)[0]
