from abc import ABC, abstractmethod
from dataclasses import dataclass
from logging import getLogger
from typing import Optional

import speech_recognition as sr
from transformers import Speech2TextForConditionalGeneration, Speech2TextProcessor

from src.speech.listen import resample, to_wav

_LOGGER = getLogger(__name__)


class SpeechToText(ABC):
    @abstractmethod
    def convert(self, audio: sr.AudioData) -> Optional[str]:
        pass


@dataclass
class SphinxSpeechToText(SpeechToText):
    recognizer: sr.Recognizer = sr.Recognizer()

    def convert(self, audio: sr.AudioData) -> Optional[str]:
        try:
            return self.recognizer.recognize_sphinx(audio)
        except sr.UnknownValueError:
            _LOGGER.exception("Sphinx could not understand audio")
        except sr.RequestError:
            _LOGGER.exception("Sphinx had a problem")
        return None


@dataclass
class S2TSpeechToText(SpeechToText):
    model: Speech2TextForConditionalGeneration
    processor: Speech2TextProcessor

    def __init__(
        self, pretrained_model: str = "facebook/s2t-small-librispeech-asr"
    ) -> None:
        self.model = Speech2TextForConditionalGeneration.from_pretrained(
            pretrained_model
        )
        self.processor = Speech2TextProcessor.from_pretrained(pretrained_model)

    def convert(self, audio: sr.AudioData) -> Optional[str]:
        wav_audio = resample(to_wav(audio), sampling_rate=16_000)
        inputs = self.processor(wav_audio, sampling_rate=16_000, return_tensors="pt")
        generated_ids = self.model.generate(
            input_ids=inputs["input_features"], attention_mask=inputs["attention_mask"]
        )
        utterances = self.processor.batch_decode(generated_ids)

        # </s> is the end of speech token
        # splitting like this separates out individual utterances
        return ". ".join(
            [
                utterance.strip()
                for entry in utterances
                for utterance in entry.split("</s>")
                if utterance.strip()
            ]
        )
