from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass

import pyttsx3


class TextToSpeech(ABC):
    @abstractmethod
    def speak(self, text: str) -> None:
        pass


@dataclass
class PyTTSTextToSpeech(TextToSpeech):
    engine: pyttsx3.engine.Engine

    @staticmethod
    def female() -> PyTTSTextToSpeech:
        engine = pyttsx3.init()
        engine.setProperty("voice", "english_rp+f3")
        return PyTTSTextToSpeech(engine)

    def speak(self, text: str) -> None:
        self.engine.say(text)
        self.engine.runAndWait()
