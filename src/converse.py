from src.chatbot import Chatbot
from src.speech.from_text import TextToSpeech
from src.speech.to_text import SpeechToText


def text_conversation(chatbot: Chatbot) -> None:
    def listen() -> str:
        return input(">>> ").casefold().strip()

    try:
        while True:
            utterance = listen()
            while not utterance:
                print("Prompt should not be empty!")
                utterance = listen()
            if utterance == "quit":
                print("ending")
                return
            response, chatbot = chatbot.respond(utterance)
            print(response)
    except KeyboardInterrupt:
        print("ending")
        return


def speech_conversation(stt: SpeechToText, chatbot: Chatbot, tts: TextToSpeech) -> None:
    from src.speech.listen import listen  # pylint: disable=import-outside-toplevel

    try:
        while True:
            print("I'm listening")
            utterance = stt.convert(listen())
            if utterance == "quit":
                print("ending")
                return
            print(f">>> {utterance}")
            response, chatbot = chatbot.respond(utterance)
            print(response)
            tts.speak(response)
    except KeyboardInterrupt:
        print("ending")
        return
