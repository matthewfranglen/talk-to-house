import typer

from src.chatbot import GPTChatbot, Settings
from src.converse import speech_conversation, text_conversation
from src.speech.from_text import PyTTSTextToSpeech
from src.speech.to_text import S2TSpeechToText

app = typer.Typer()


@app.command()
def text(
    *,
    personality: str = "i am a house. i have organs. i love my son.",
    min_length: int = 1,
    max_length: int = 20,
    max_history: int = 2,
    temperature: float = 0.7,
    top_k: int = 0,
    top_p: float = 0.9,
    no_sample: bool = False,
) -> None:
    settings = Settings(
        min_length=min_length,
        max_length=max_length,
        max_history=max_history,
        temperature=temperature,
        top_k=top_k,
        top_p=top_p,
        no_sample=no_sample,
    )
    chatbot = GPTChatbot.make(personality=personality, settings=settings)

    text_conversation(chatbot)


@app.command()
def speech(
    *,
    personality: str = "i am a house. i have organs. i love my son.",
    min_length: int = 1,
    max_length: int = 20,
    max_history: int = 2,
    temperature: float = 0.7,
    top_k: int = 0,
    top_p: float = 0.9,
    no_sample: bool = False,
) -> None:
    settings = Settings(
        min_length=min_length,
        max_length=max_length,
        max_history=max_history,
        temperature=temperature,
        top_k=top_k,
        top_p=top_p,
        no_sample=no_sample,
    )
    chatbot = GPTChatbot.make(personality=personality, settings=settings)
    tts = PyTTSTextToSpeech.female()
    stt = S2TSpeechToText()

    speech_conversation(stt=stt, chatbot=chatbot, tts=tts)


if __name__ == "__main__":
    app()
